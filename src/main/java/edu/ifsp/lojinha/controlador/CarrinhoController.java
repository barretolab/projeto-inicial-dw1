package edu.ifsp.lojinha.controlador;

import java.io.IOException;
import java.util.List;

import edu.ifsp.lojinha.modelo.Produto;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/carrinho.sec")
public class CarrinhoController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String cmd = request.getParameter("cmd");

		HttpSession session = request.getSession();
		List<Produto> carrinho = (List<Produto>)session.getAttribute("carrinho");
		
		if ("remove".equals(cmd)) {
			String paramProduto = request.getParameter("produto");
			int id = Integer.parseInt(paramProduto);

			if (carrinho != null) {
				carrinho.removeIf(p -> p.getId() == id);
			}			
			
		}
				
		/* Havia um comando na URL? */
		if (cmd != null) {
			response.sendRedirect("carrinho.sec");
		} else {		
			RequestDispatcher rd = request.getRequestDispatcher("carrinho.jsp");
			rd.forward(request, response);
		}		
	}

}

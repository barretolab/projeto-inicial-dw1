package edu.ifsp.lojinha.controlador.helpers;

import edu.ifsp.lojinha.modelo.Carrinho;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class ListaProdutosHelper implements CommandHelper {

	@Override
	public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		Carrinho carrinho = (Carrinho) request.getAttribute("carrinho");

		if (carrinho != null) {
			carrinho.ordenaProdutos();
			session.setAttribute("carrinho", carrinho);
		}

		return "/listagem.jsp";
	}

}

package edu.ifsp.lojinha.controlador.helpers;

import java.util.Arrays;
import java.util.List;

import edu.ifsp.lojinha.modelo.Carrinho;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class RemoveProdutoHelper implements CommandHelper {

	@Override
	public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<String> checked = Arrays.asList(request.getParameterValues("remove"));
		// checked.forEach(s -> System.out.println(s));
		HttpSession session = request.getSession();

		Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");
		if (carrinho != null && checked != null) {
			carrinho.removerProdutos(checked);
		}

		return "listagem.jsp";
	}

}

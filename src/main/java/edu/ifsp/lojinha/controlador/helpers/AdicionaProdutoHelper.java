package edu.ifsp.lojinha.controlador.helpers;

import edu.ifsp.lojinha.modelo.Carrinho;
import edu.ifsp.lojinha.modelo.Produto;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class AdicionaProdutoHelper implements CommandHelper {

@Override
public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
	Produto novoProduto = new Produto(request.getParameter("nome"));
	HttpSession session = request.getSession();

	Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");
	if (carrinho == null) {
		carrinho = new Carrinho();
	}

	carrinho.inserirProduto(novoProduto);
	session.setAttribute("carrinho", carrinho);

	return "/adicionado.jsp";
}

}

package edu.ifsp.lojinha.controlador.helpers;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface CommandHelper {
	public abstract String executar(HttpServletRequest request, HttpServletResponse response) throws Exception;

}

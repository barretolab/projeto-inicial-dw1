package edu.ifsp.lojinha.controlador.servelets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import edu.ifsp.lojinha.modelo.Carrinho;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/ServletRemocao")
public class ServletRemocao extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ServletRemocao() {
    }

    //RESPONSABILIDADE: Remo��o de um ou mais produtos do carrinho
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> checked = Arrays.asList(request.getParameterValues("remove"));
//		checked.forEach(s -> System.out.println(s));
		HttpSession session = request.getSession();
		Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

		if(carrinho != null && checked != null) {
			carrinho.removerProdutos(checked);
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/listar.jsp");
		dispatcher.forward(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}

package edu.ifsp.lojinha.controlador.servelets;

import java.io.IOException;

import edu.ifsp.lojinha.modelo.Carrinho;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/ServletListagem")
public class ServletListagem extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ServletListagem() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	Carrinho carrinho = (Carrinho) request.getAttribute("carrinho");
    	
    	if(carrinho != null) {
    		carrinho.ordenaProdutos();
    		session.setAttribute("carrinho", carrinho);
    	}
    	
    	RequestDispatcher dispatcher = request.getRequestDispatcher("/listar.jsp");
		dispatcher.forward(request,response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

}

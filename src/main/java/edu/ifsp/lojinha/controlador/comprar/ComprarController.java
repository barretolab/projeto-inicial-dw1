package edu.ifsp.lojinha.controlador.comprar;

import java.io.IOException;

import edu.ifsp.lojinha.controlador.Command;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/comprar.sec")
public class ComprarController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String cmd = request.getParameter("cmd"); 		
		Command command = Factory.getCommand(cmd);
		command.processar(request, response);
	}

}

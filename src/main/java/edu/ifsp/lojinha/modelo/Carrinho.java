package edu.ifsp.lojinha.modelo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Carrinho {

	private final List<Produto> produtos = new ArrayList<>();

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void inserirProduto(Produto produto) {
		produtos.add(produto);
	}

	public void removerProdutos(List<String> ids) {
		List<Produto> produtos = this.getProdutos();
		produtos.removeIf(p -> ids.contains(String.valueOf(p.getId())));
	}

	public void ordenaProdutos() {
		produtos.sort(new Comparator<Produto>() {
			@Override
			public int compare(Produto o1, Produto o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
	}
}

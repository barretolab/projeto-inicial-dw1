<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset="UTF-8" />
		<title>Carrinho</title>
	</head>
	<body>
		Produto ${param.nome} adicionado com sucesso!
		<br />
		<a href="index.jsp">Voltar</a>
	</body>
</html>